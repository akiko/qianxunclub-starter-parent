package com.qianxunclub.starter.eureka.autoconfigure;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author chihiro.zhang
 */
@Slf4j
@Configuration
@EnableConfigurationProperties({EurekaProperties.class})
@ConditionalOnProperty(value = "eureka.enabled", matchIfMissing = true)
@EnableEurekaClient
public class EurekaConfiguration extends WebMvcConfigurerAdapter{

    @Autowired
    private EurekaProperties eurekaProperties;


}
