package com.qianxunclub.starter.eureka.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author chihiro.zhang
 */
@Data
@Component
@ConfigurationProperties(prefix = "eureka")
public class EurekaProperties {

    private Boolean enabled;
}

