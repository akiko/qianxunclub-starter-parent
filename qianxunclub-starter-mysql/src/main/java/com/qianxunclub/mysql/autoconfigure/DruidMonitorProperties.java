package com.qianxunclub.mysql.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author chihiro.zhang
 */
@ConfigurationProperties(prefix = "druid")
public class DruidMonitorProperties {

    /**
     * 白名单
     */
    private String allow = "127.0.0.1";

    /**
     * IP黑名单(存在共同时，deny优先于allow)
     */
    private String deny = "";

    /**
     * 登录查看信息的账号
     */
    private String loginUsername = "admin";

    /**
     * 登录查看信息的密码
     */
    private String loginPassword = "admin";

    public String getAllow() {
        return allow;
    }

    public void setAllow(String allow) {
        this.allow = allow;
    }

    public String getDeny() {
        return deny;
    }

    public void setDeny(String deny) {
        this.deny = deny;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }
}

