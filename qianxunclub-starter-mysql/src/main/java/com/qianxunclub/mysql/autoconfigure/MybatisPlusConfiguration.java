package com.qianxunclub.mysql.autoconfigure;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author chihiro.zhang
 */
@Configuration
@ConditionalOnProperty(prefix = "mybatis-plus.generic", name = "enabled", havingValue = "true")
public class MybatisPlusConfiguration {

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        return paginationInterceptor;
    }
}
