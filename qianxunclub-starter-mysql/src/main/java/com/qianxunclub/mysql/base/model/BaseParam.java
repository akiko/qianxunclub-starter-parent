package com.qianxunclub.mysql.base.model;

import lombok.Data;

/**
 * @author chihiro.zhang
 */
@Data
public class BaseParam {
    private Integer enabled;
    private Integer deleted;
    private Integer current;
    private Integer size = 10;
    private String orderByField;
    private boolean asc;
}
