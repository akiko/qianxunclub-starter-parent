package com.qianxunclub.mysql.base.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author chihiro.zhang
 */
@Data
public class IdParam {
    @NotNull
    private Integer id;
}
